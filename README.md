Login sencillo pero con el uso de:
    width: MediaQuery.of(context).size.width,
    height: MediaQuery.of(context).size.height,
  para que se los limites del contenerod sean los mismos de la pantalla

  Ademas, utilizo una sola funcion/clase para mostrar dos TextField que 
  capturan la informacion escrita por el usuario.

  Tambien se hace uso del widget "Align" para crear un circulo que se 
  puede usar como un efecto y que se muestra detras de los inputs y
  el logo.

Así quedó la pantalla inicial:

![Pantalla Inicial](assets/Screenshot_2019-11-25-17-26-48.png)
![Pantalla con el teclado activo](assets/Screenshot_2019-11-25-17-26-55.png)
# login_app

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
