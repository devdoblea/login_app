import 'package:flutter/material.dart';
import 'package:login_app/custom_input_field.dart';
 
void main() => runApp(MyApp());
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: BodyLogin(),
    );
  }
}

class BodyLogin extends StatelessWidget {
  const BodyLogin({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        color: Colors.blue,
        child: Stack(
          children: <Widget>[
            Align(
              alignment: Alignment.bottomRight,
              widthFactor: 1.36,
              heightFactor: 1.36,
              child: Material(
                borderRadius:BorderRadius.all(Radius.circular(200.0)),
                color: Color.fromRGBO(255, 255, 255, 0.4),
                child: Container(
                  width: 380,
                  height: 380,
                ),
              ),
            ),
            Center(
              child: Container(
                width: 400.0,
                height: 400.0,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Material(
                      elevation: 10.0,
                      borderRadius: BorderRadius.all(Radius.circular(60.0)),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Image.asset(
                          'assets/images/icono_flutter.png',
                          width: 100.0,
                          height: 100.0,
                        ),
                      ),
                    ),
                    // Con este widget puedo llamar cualquier cantidad 
                    // de input. solo debo configurarlo en el constructor
                    // de este mismo widget
                    CustomInputField(
                      Icon(Icons.person, color: Colors.white), 'Usuario'
                    ),
                    CustomInputField(
                      Icon(Icons.lock, color: Colors.white), 'Contraseña'
                    ),
                    Container(
                      width: 150,
                      child: RaisedButton(
                        onPressed: () {},
                        color: Colors.deepOrange,
                        textColor: Colors.white,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(
                            Radius.circular(10.0),
                          )
                        ),
                        child: Text(
                          'Login',
                          style: TextStyle(
                            fontSize: 20.0
                          )
                        )
                      ),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}